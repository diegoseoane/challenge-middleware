const express = require("express");
const { logPath, logMethod, showCursos } = require("./functions.js");
const app = express();

app.use(logPath);
app.use(logMethod);

app.get("/cursos", showCursos);

app.listen(3000, function () {
    console.log("servidor funcionando");
})
