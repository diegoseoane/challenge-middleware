const cursos = [{"id": 1, "nombreCurso": "Back End"},
    {"id": 2, "nombreCurso": "Front End"},
    {"id": 3, "nombreCurso": "FullStack"}]

function logPath(req, res, next) {
    console.log(req.path);
    next();
}

function logMethod(req, res, next) {
    console.log(`request HTTP method: ${req.method}`);
    next();
}

function showCursos(req, res) {
    res.send(cursos);
}

module.exports = {
    logPath,
    logMethod,
    showCursos
}